﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace HomeCommon
{
    class BlobInformation
    {
        public Uri BlobUri { get; set; }

        public string BlobName
        {
            get { return BlobUri.Segments[BlobUri.Segments.Length - 1]; }
        }

        public string BlobNameWithoutExtension
        {
            get { return Path.GetFileNameWithoutExtension(BlobName); }
        }

        public int MyId { get; set; }
    }
}
